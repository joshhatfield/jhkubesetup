#/bin/bash

# This is used with my test vagrant env  apiserver id will change with deployments.

yes | sudo kubeadm reset

# For flannel


sudo sysctl net.bridge.bridge-nf-call-iptables=1

sudo kubeadm init --pod-network-cidr=10.244.0.0/16  --apiserver-advertise-address=$(hostname -i | awk '{print $2}')

kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/v0.10.0/Documentation/kube-flannel.yml


# Calico setup



sudo kubeadm init --pod-network-cidr=192.168.0.0/16 --apiserver-advertise-address=$(hostname -i | awk '{print $2}')


kubectl apply -f https://docs.projectcalico.org/v3.1/getting-started/kubernetes/installation/hosted/rbac-kdd.yaml
kubectl apply -f https://docs.projectcalico.org/v3.1/getting-started/kubernetes/installation/hosted/kubernetes-datastore/calico-networking/1.7/calico.yaml


## Wait until all are running 

watch kubectl get pods --all-namespaces

## Taint and reload 

kubectl taint nodes --all node-role.kubernetes.io/master-

## Wait until ready 

kubectl get nodes -o wide

watch kubectl get pods --all-namespaces


# To setup a non sudo user 
  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

# NOTE  The $HOME/.kube/config is EXTREMELY important contains your kube port config, without this your kube cli will not work do this on your root and user acct. 


# install a dashboard


kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml

# Create an admin user token for dashboard

tee $HOME/admin-svc-acct.yaml <<EOF
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kube-system
EOF

kubectl apply -f $HOME/admin-svc-acct.yaml

tee $HOME/admin-svc-acct-secret.yaml <<EOF
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin

subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kube-system
EOF

kubectl apply -f $HOME/admin-svc-acct-secret.yaml

kubectl -n kube-system describe secret \
$(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')


# Serve on port of choice with --port=80 default is 8001
kubectl proxy 

# for vagrant environments you want to set the address to non localhost 

kubectl proxy --address='0.0.0.0'


# You can now access on http://localhost:8081/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/  or fowarded port


http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/

# add a node with the join command

#Test a container deploy

kubectl run nginx --image=nginx --replicas=3

# Check network overlay is working

kubectl get pods -o wide